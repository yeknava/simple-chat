<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yeknava\SimpleChat\CanChat;

class UserModel extends Model
{
    use SoftDeletes, CanChat;

    protected $table = 'simple_chat_test_users';
}
