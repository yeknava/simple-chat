<?php

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Support\Facades\Config;
use Tests\UserModel;
use Tests\TopicModel;
use Orchestra\Testbench\TestCase;
use Yeknava\SimpleChat\DTOs\ChatPermissionDTO;
use Yeknava\SimpleChat\DTOs\MediaDTO;
use Yeknava\SimpleChat\Exceptions\AlreadyReactedException;
use Yeknava\SimpleChat\SimpleChat;
use Yeknava\SimpleChat\Models\ChatMedia;
use Yeknava\SimpleChat\Models\ChatMember;
use Yeknava\SimpleChat\Models\ChatMessage;
use Yeknava\SimpleChat\Exceptions\WrongChatException;
use Yeknava\SimpleChat\Exceptions\PermissionException;
use Yeknava\SimpleChat\Exceptions\ExpiredPlanException;
use Yeknava\SimpleChat\Exceptions\NotStartedPlanException;
use Yeknava\SimpleChat\Exceptions\MembershipLimitException;
use Yeknava\SimpleChat\Exceptions\OwnerReactionException;
use Yeknava\SimpleChat\Exceptions\PlanQuantityLimitException;
use Yeknava\SimpleChat\Models\Chat;
use Yeknava\SimpleChat\Models\ChatPaymentPlan;
use Yeknava\SimpleChat\Models\ChatTag;

class ChatTest extends TestCase
{
    use RefreshDatabase;

    /**
     * Setup the test environment.
     */
    protected function setUp(): void
    {
        parent::setUp();

        $this->loadMigrationsFrom(__DIR__ . '/../migrations', ['--database' => 'testing']);
        $this->loadMigrationsFrom(__DIR__ . '/migrations', ['--database' => 'testing']);
    }
    /**
     * Define environment setup.
     *
     * @param  \Illuminate\Foundation\Application  $app
     * @return void
     */
    protected function getEnvironmentSetUp($app)
    {
        $app['config']->set('database.testing', 'pgsql');

        $app['config']->set('simple-chat', require(__DIR__ . '/../config/simple-chat.php'));
    }

    protected function getPackageProviders($app)
    {
        return [
            \Yeknava\SimpleChat\SimpleChatServiceProvider::class,
        ];
    }

    public function init()
    {
        $user = (new UserModel([]));
        $user->save();

        $source = (new TopicModel([]));
        $source->save();

        return $user;
    }

    public function test()
    {
        $user = $this->init();

        $user2 = (new UserModel([]));
        $user2->save();

        $user3 = (new UserModel([]));
        $user3->save();

        $guest = (new UserModel([]));
        $guest->save();

        $admin = (new UserModel([]));
        $admin->save();

        $directChat = $user->newDirectChat([
            'title' => 'direct chat test'
        ]);

        try {
            $directChat->newMessage(null); //except exception
            $this->assertTrue(false);
        } catch (Throwable $e) {
            $this->assertInstanceOf(PermissionException::class, $e);
        }

        $sysMessage = $directChat->newSystemMessage('test system message #first_tag #second_tag #third');
        $this->assertEquals($sysMessage->tags()->count(), 3);

        $memberDirect1 = $directChat->members()->first();
        $this->assertEquals($memberDirect1->role, ChatMember::ROLE_OWNER);

        $memberDirect2 = $directChat->addMember($user2);

        $this->assertEquals(2, $directChat->members()->count());

        $message = $directChat->newMessage(
            $memberDirect1,
            'test message',
        );

        $replyMessage = $message->reply(
            $memberDirect2,
            'test reply',
        );

        $message->reply(
            $memberDirect2,
            'test reply 2',
        );

        $message->reply(
            $memberDirect2,
            'test reply 3',
        );

        $this->assertSame($replyMessage->body, 'test reply');

        $this->assertEquals($message->id, $replyMessage->replyTo->id);

        $this->assertEquals(5, $directChat->messages()->count());

        $reaction = $message->react($memberDirect2, 1);

        $replyMessage->react($memberDirect1, 2);

        try {
            $message->react($memberDirect2, 1); //except exception
            $this->assertTrue(false);
        } catch (Throwable $e) {
            $this->assertInstanceOf(AlreadyReactedException::class, $e);
            /** @var AlreadyReactedException $e */
            $this->assertSame($reaction->id, $e->getReaction()->id);
        }

        Config::set('simple-chat.reaction_behavior', 'infinite');

        $reaction = $message->react($memberDirect2, 1);
        $reaction = $message->react($memberDirect2, 1);
        $reaction = $message->react($memberDirect2, 1);
        $reaction = $message->react($memberDirect2, 2);
        $reaction = $message->react($memberDirect2, 2);

        $totalReactions = (new SimpleChat())->messagesTotalReactions([$message->id], $memberDirect2->id);
        $this->assertEquals(4, $totalReactions['total_reactions'][0]->total);

        try {
            $directChat->addMember($user3); //except exception
            $this->assertTrue(false);
        } catch (Throwable $e) {
            $this->assertInstanceOf(MembershipLimitException::class, $e);
        }

        $groupChat = $user->newGroupChat([
            'title' => 'group chat test'
        ]);
        $owner = $groupChat->members()->first();

        $this->assertEquals($owner->role, ChatMember::ROLE_OWNER);

        $memberGroup2 = $groupChat->addMember($user2);
        $memberGroup3 = $groupChat->addMember($user3);

        $groupChat->addMember($user2); // already member of group chat

        $this->assertEquals(3, $groupChat->members()->count());

        $message = $groupChat->newMessage(
            $owner,
            'test message',
        );

        try {
            $groupChat->newMessage($memberDirect1); //except exception
            $this->assertTrue(false);
        } catch (Throwable $e) {
            $this->assertInstanceOf(PermissionException::class, $e);
        }

        $mediaDTO = new MediaDTO(
            __DIR__ . '/UserModel.php',
            ChatMedia::TYPE_FILE,
            'UserModel.php',
            true
        );

        $message = $groupChat->newMessage(
            $owner,
            'test message',
            ChatMessage::TYPE_FILE,
            false,
            null,
            $mediaDTO
        );

        $this->assertNotNull($message->media);
        $this->assertEquals('UserModel.php', $message->media->filename);

        $message = $groupChat->newMessage($memberGroup2, 'test member 2 message');

        $this->assertEquals(3, $groupChat->messages()->count());

        try {
            $groupChat->deleteMessage($message, $memberGroup3);
            $this->assertTrue(false);
        } catch (Throwable $e) {
            $this->assertInstanceOf(PermissionException::class, $e);
        }

        $this->assertEquals(3, $groupChat->messages()->count());

        $sm = new SimpleChat();
        $sm->setChatById($directChat->id);
        $sm->setUser($user);

        try {
            $sm->newMessageById(
                'reply',
                ChatMessage::TYPE_MESSAGE,
                false,
                $message->id
            ); //passing $message from another chat ($groupChat)
            $this->assertTrue(false);
        } catch (Throwable $e) {
            $this->assertInstanceOf(WrongChatException::class, $e);
        }

        $sm->deleteMessageById($message->id, $memberGroup2->id);
        $this->assertEquals(2, $groupChat->messages()->count());

        $this->assertEquals(true, $sm->isMember($memberDirect2));
        $this->assertEquals(false, $sm->isMember($memberGroup3));

        $permissions = new ChatPermissionDTO();
        $permissions->sendMessage = false;
        $permissions->allowSpectators = false;
        // testing channel
        $sm = new SimpleChat();
        $sm->setUser($user);
        $sm->newChannelChat(
            [
                'title' => 'test channel',
                'permissions' => $permissions
            ],
            TopicModel::first()
        );

        $expiredPlan = $sm->newPaymentPlan([
            'title' => 'special plan',
            'price' => 1000,
            'quantity' => 1,
            'plan_started_at' => now()->subDays(2),
            'plan_expired_at' => now()->subDays(1)
        ], $user2);

        try {
            $sm->addMemberById(
                $user2->id,
                $user->getMorphClass(),
                ChatMember::ROLE_MEMBER,
                null,
                $expiredPlan->id
            );
            $this->assertTrue(false);
        } catch (Throwable $e) {
            $this->assertInstanceOf(ExpiredPlanException::class, $e);
        }

        $futurePlan = $sm->newPaymentPlan([
            'title' => 'special plan',
            'price' => 1000,
            'quantity' => 1,
            'plan_started_at' => now()->addMinute(),
            'plan_expired_at' => now()->addMinutes(3)
        ], $user2);

        try {
            $sm->addMemberById(
                $user2->id,
                $user2->getMorphClass(),
                ChatMember::ROLE_MEMBER,
                null,
                $futurePlan->id
            );
            $this->assertTrue(false);
        } catch (Throwable $e) {
            $this->assertInstanceOf(NotStartedPlanException::class, $e);
        }

        $correctPlan = $sm->newPaymentPlan([
            'title' => 'special plan',
            'price' => 1000,
            'quantity' => 1,
            'membership_expired_days_after' => 5,
            'plan_started_at' => now()->subMinute(),
            'plan_expired_at' => now()->addMinutes(3)
        ], $user2);

        $channelMember1 = $sm->addMemberById(
            $user2->id,
            $user2->getMorphClass(),
            ChatMember::ROLE_MEMBER,
            null,
            $correctPlan->id
        );
        $this->assertEquals(now()->addDays(5)->toDateTimeString(), $channelMember1->expired_at->toDateTimeString());
        $sm->isMemberById($channelMember1->id);
        $correctPlan = $correctPlan->refresh();

        try {
            $sm->addMember($user3, ChatMember::ROLE_MEMBER, null, $correctPlan);
            $this->assertTrue(false);
        } catch (Throwable $e) {
            $this->assertInstanceOf(PlanQuantityLimitException::class, $e);
        }

        $sm->setUser($guest);

        $this->assertFalse($sm->getChat()->canView());

        $sm->getChat()->update([
            'permissions' => 
                array_merge(
                    $sm->getChat()->permissions,
                    [
                        'allow_spectators' => true
                    ]
                )
        ]);

        $this->assertTrue($sm->getChat()->canView());
    }

    public function testPublicChannels()
    {
        $user = $this->init();

        $user2 = (new UserModel([]));
        $user2->save();

        $user3 = (new UserModel([]));
        $user3->save();

        $guest = (new UserModel([]));
        $guest->save();

        $admin = (new UserModel([]));
        $admin->save();

        $topic = new TopicModel();
        $topic->save();

        $sm = new SimpleChat();

        $permissions = new ChatPermissionDTO();
        $permissions->allowSpectators = true;
        $permissions->reviewNeeded = true;

        $group = $topic->newGroupChat([
            'title' => 'test moderated group',
            'permissions' => $permissions
        ], $user);

        $this->assertSame($group->id, $topic->chats()->first()->id);
        $this->assertSame(Chat::TYPE_GROUP, $group->type);
        $this->assertTrue($group->permissions['allow_spectators']);

        $member2 = $group->addMember($user2);
        $member3 = $group->addMember($user3, ChatMember::ROLE_MEMBER, $member2);

        $this->assertEquals($member2->id, $member3->invitedBy->id);

        $mediaDTO = new MediaDTO('https://google.com', ChatMedia::TYPE_URL);

        $message1 = $group->newMessage(
            $group->owner,
            'test first message #first_tag #second_tag',
            ChatMessage::TYPE_MESSAGE,
            true,
            null,
            $mediaDTO
        );

        $this->assertEquals('https://google.com', $message1->url);
        $this->assertTrue($message1->anonymous);

        $this->assertEquals(2, $message1->tags()->count());

        $message1 = $message1->edit(null, null);
        $this->assertEquals('https://google.com', $message1->url);
        $mediaDTO = new MediaDTO(null, ChatMedia::TYPE_URL);
        $message1 = (new SimpleChat($group))
            ->setUser($group->owner)
            ->editMessage(
                $message1->id,
                null,
                null,
                ChatMessage::TYPE_MESSAGE,
                false,
                $mediaDTO
            );
        $this->assertEquals(null, $message1->url);

        $this->assertEquals(0, $message1->tags()->count());
        $this->assertNull($message1->body);
        $this->assertEquals(2, ChatTag::all()->count());

        $message2 = $message1->reply($member2, 'test second message'); //message1 first reply
        $message3 = $message1->reply($member3, 'test third message');
        $message4 = $message1->reply($member3, 'test forth message');
        $message5 = $message1->reply($member3, 'test fifth message'); //message1 4th reply

        $message6 = $message2->reply($group->owner, 'test sixth message');

        $this->assertEquals(0, $message1->totalReplies()); //in moderated groups member's messages wont publish by themselves
        
        $message2->publish($group->owner);
        $message3->publish($group->owner);
        $message4->publish($group->owner);
        $message5->publish($group->owner);

        $this->assertEquals(4, $message1->totalReplies());

        $message1->react($member2, 1); //message1 type(1) first reaction
        $message1->react($member3, 1); //message1 type(1) second reaction

        $message2->react($group->owner, 2);
        $message2->react($member3, 1);

        try {
            $message2->react($member2, 1);
            $this->assertTrue(false);
        } catch (Throwable $e) {
            $this->assertInstanceOf(OwnerReactionException::class, $e);
        }

        $reactions = $sm->messagesTotalReactions([$message1->id, $message2->id, $message3->id], $member2->id, true);

        $this->assertEquals(
            4, 
            collect($reactions['total_replies'])->filter(function($value, $key) use ($message1) {
                return $value['message_id'] == $message1->id;
            })->first()['replies']
        );

        $this->assertEquals(
            2, 
            collect($reactions['total_reactions'])->filter(function($value, $key) use ($message1) {
                return $value['message_id'] == $message1->id && $value['type'] == 1;
            })->first()['total']
        );
    }
}
