<?php

namespace Tests;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yeknava\SimpleChat\ChatTopic;

class TopicModel extends Model
{
    use SoftDeletes, ChatTopic;

    protected $table = 'simple_chat_test_topic';
}
