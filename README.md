# Laravel Simple Chat Package

Laravel Simple Chat Package

## Installation

Use the package manager [composer](https://getcomposer.org/) to install simple chat package.

```bash
composer require yeknava/simple-chat
```

## Usage
Run this command in your terminal:

```bash
php artisan vendor:publish
```

Add CanChat trait to models that may initiate chats (normally User model).

```php
<?php

use Yeknava\SimpleChat\CanChat;

class User extends Model {
    use CanChat;
}
```

You may also add ChatTopic to models that may be chat topics

```php
<?php

use Yeknava\SimpleChat\ChatTopic;

class Post extends Model {
    use ChatTopic;
}
```

Then You can create chats with one of these ways 

```php

$chat = $user->newDirectChat(
    [
        'title' => 'post discussion',
        'description' => 'a chat about this specific post'
    ],
    $post // or null if this chat has no references
);

// or

$chat = $post->newDirectChat(
    [
        'title' => 'post discussion',
        'description' => 'a chat about this specific post'
    ],
    $user // or null if this chat has no creator (may created by system)
);
```
In case you don't like using traits or don't access models you may still create chats with SimpleChat class

```php
use Yeknava\SimpleChat\SimpleChat;

$simpleChat = new SimpleChat();

$chat = $simpleChat->setUser($user)->newDirectChat(
    [
        'title' => 'post discussion',
        'description' => 'a chat about this specific post',
        'category' => 'recent'
    ],
    $post
);
```

There are 3 type of chats:
- Direct: chat between 2 member
- Group: chat between several members (you may set limits in config file)
- Channel: channel is like groups which many members may join but only admins and owner can post messages in that

```php
use Yeknava\SimpleChat\SimpleChat;
use Yeknava\SimpleChat\SimpleChat\DTOs\ChatPermissionDTO;

$simpleChat = new SimpleChat();

$directChat = $simpleChat->setUser($user)->newDirectChat(
    [
        'title' => 'post discussion',
        'description' => 'a chat about this specific post'
    ],
    $post
);

$groupChat = $simpleChat->setUser($user)->newGroupChat(
    [
        'title' => 'post discussion',
        'description' => 'a chat about this specific post',
        'permissions' => (new ChatPermissionDTO())
    ],
    $post
);

$channelChat = $simpleChat->setUser($user)->newChannelChat(
    [
        'title' => 'post discussion',
        'description' => 'a chat about this specific post'
    ],
    $post
);

```

Add new member to chats:

```php
use Yeknava\SimpleChat\Models\ChatMember;

$member = $chat->addMember($user2, ChatMember::ROLE_MEMBER);
```

Post messages to chats:

```php
use Yeknava\SimpleChat\Models\ChatMessages;
use Yeknava\SimpleChat\SimpleChat;

$member = (new SimpleChat())->setUser($user)->findChatMember($chat);

$message = $member->message(
    'hi', //body
    ChatMessage::TYPE_MESSAGE, //type
    false, //anonymous (bool)
    null, //reply to (ChatMessage)
    null, //media (MediaDTO)
);

// or 

$message = (new SimpleChat($chat))->setUser($user)->newMessage(
    'hi', //body
    ChatMessage::TYPE_MESSAGE, //type
    false, //anonymous (bool)
    null, //reply to (ChatMessage)
    null, //media (MediaDTO)
);
```
React to a message:

```php
use Yeknava\SimpleChat\Models\ChatMessages;
use Yeknava\SimpleChat\SimpleChat;

$member = (new SimpleChat())->setUser($user)->findChatMember($chat);

$reaction = $message->react(
    $member,
    1 //reaction type (you can specify types in config)
);

// or 

$message = (new SimpleChat())->react(
    $member,
    1
);
```

Get message total reaction

```php
use Yeknava\SimpleChat\SimpleChat;

(new SimpleChat())->totalReactions($message);

/** 
 *  get 'total_reactions' and 'total_replies' for list of messages, and
 *  'member_reactions' for a specific member. it is highly recommended to use
 *  this method when you return list of messages instead of getting reactions and
 *  replies for each message.
 */
(new SimpleChat())->messagesTotalReactions(
    [
        $message->id
    ],
    $member->id,
);
```

Check if user can view a chat or not (depends on chat's type, in groups and direct chats only members may view them and in channels it depends on allow_spectators field):

```php
use Yeknava\SimpleChat\SimpleChat;

(new SimpleChat())->setUser($user)->cantView($chat);
```

Editing message by user or admin:

```php
use Yeknava\SimpleChat\SimpleChat;

(new SimpleChat($chat))
    ->setUser($user)
    ->edit(
        $message1->id,
        null,
        null,
        ChatMessage::TYPE_MESSAGE,
        false,
        $mediaDTO
    );
```

## Config
```php
<?php

return [
    'use_reaction' => true, // you may disable reaction feature
    /*
     *  you may disable tagging feature.
     *  message tagging automatically works on messages
     *  which have # (hashtag) in their message bodies.
     */
    'use_tag' => true,

    'media_storage_disk' => 'local',
    'media_default_path' => storage_path('app/chat/media'),

    /**
     * there are 3 supported reaction's behaviors:
     *      - infinite: user can react as many as they want
     *          (a user may like a message many times)
     *      - per_type: user can only react once per type
     *          (a user may like a message and favorite same message as well)
     *      - once: user can only react once per message
     *          (a user may only like or favorite a message)
     */
    'reaction_behavior' => 'once', 
    'reaction_types' => [
        1 => 'clap',
        2 => 'like',
        3 => 'favorite',
    ],

    /**
     * if false members can not react to self's messages
     */
    'react_to_self_messages' => false
];

```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.



## License
[MIT](https://choosealicense.com/licenses/mit/)
