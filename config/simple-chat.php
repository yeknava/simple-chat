<?php

return [
    'use_reaction' => true, // you may disable reaction feature
    /*
     *  you may disable tagging feature.
     *  message tagging automatically works on messages
     *  which have # (hashtag) in their message bodies.
     */
    'use_tag' => true,

    'media_storage_disk' => 'local',
    'media_default_path' => storage_path('app/chat/media'),

    /**
     * there are 3 supported reaction's behaviors:
     *      - infinite: user can react as many as they want
     *          (a user may like a message many times)
     *      - per_type: user can only react once per type
     *          (a user may like a message and favorite same message as well)
     *      - once: user can only react once per message
     *          (a user may only like or favorite a message)
     */
    'reaction_behavior' => 'once', 
    'reaction_types' => [
        1 => 'clap',
        2 => 'like',
        3 => 'favorite',
    ],

    /**
     * if false members can not react to self's messages
     */
    'react_to_self_messages' => false
];
