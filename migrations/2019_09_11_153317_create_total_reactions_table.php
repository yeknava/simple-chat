<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTotalReactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('simple-chat.use_reaction') === false) {
            return;
        }

        Schema::create('chat_total_reactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type')->unsigned();
            $table->bigInteger('total')->unsigned();
            $table->bigInteger('message_id')->unsigned();
            $table->foreign('message_id')->references('id')->on('chat_messages');

            $table->unique(['type', 'message_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (config('simple-chat.use_reaction') === false) {
            return;
        }
        
        Schema::dropIfExists('chat_total_reactions');
    }
}
