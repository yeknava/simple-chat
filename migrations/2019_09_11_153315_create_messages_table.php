<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Yeknava\SimpleChat\Models\ChatMember;
use Yeknava\SimpleChat\Models\ChatMessage;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_messages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->text('body')->nullable();
            $table->string('type')->default(ChatMessage::TYPE_MESSAGE);
            $table->string('member_role')->default(ChatMember::ROLE_MEMBER);
            $table->text('url')->nullable();
            $table->boolean('anonymous')->default(false);
            $table->boolean('pinned')->default(false);
            $table->bigInteger('reply_to_message_id')->unsigned()->nullable();
            $table->foreign('reply_to_message_id')->references('id')->on('chat_messages');
            $table->bigInteger('chat_id')->unsigned();
            $table->foreign('chat_id')->references('id')->on('chats');
            $table->bigInteger('member_id')->unsigned()->nullable();
            $table->foreign('member_id')->references('id')->on('chat_members');
            $table->bigInteger('media_id')->unsigned()->nullable();
            $table->foreign('media_id')->references('id')->on('chat_media');
            $table->bigInteger('published_by')->unsigned()->nullable();
            $table->foreign('published_by')->references('id')->on('chat_members');
            $table->jsonb('extra')->nullable();
            $table->timestamp('published_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_messages');
    }
}
