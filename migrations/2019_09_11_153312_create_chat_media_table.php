<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateChatMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_media', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('type');
            $table->string('driver')->nullable();
			$table->string('filename', 255)->nullable();
			$table->string('extension', 6);
			$table->string('mimetype', 30);
			$table->string('path');
			$table->string('sha1', 40);
			$table->bigInteger('filesize')->unsigned();
			$table->string('uploader_ip', 45);
            $table->bigInteger('chat_id')->unsigned();
            $table->foreign('chat_id')->references('id')->on('chats');
            $table->jsonb('extra')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });

        Schema::table('chats', function (Blueprint $table) {
            $table->foreignId('photo_id')->nullable()->constrained('chat_media')->after('reference');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        try {
            Schema::table('chats', function (Blueprint $table) {
                $table->dropConstrainedForeignId('photo_id');
            });
        } catch (Throwable $e) {
            // in case running migration on sqlite connection
        }

        Schema::dropIfExists('chat_media');
    }
}
