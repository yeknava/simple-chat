<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMessageTagTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('simple-chat.use_tag') === false) {
            return;
        }

        Schema::create('chat_message_tag', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('tag_id')->unsigned();
            $table->foreign('tag_id')->references('id')->on('chat_tags');
            $table->bigInteger('message_id')->unsigned();
            $table->foreign('message_id')->references('id')->on('chat_messages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (config('simple-chat.use_tag') === false) {
            return;
        }
        
        Schema::dropIfExists('chat_message_tag');
    }
}
