<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Yeknava\SimpleChat\Models\ChatMember;

class CreateChatPaymentPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('chat_payment_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title')->nullable();
            $table->string('currency', 3)->nullable();
            $table->double('price', 16, 4);
            $table->string('membership_role')->default(ChatMember::ROLE_MEMBER);
            $table->smallInteger('membership_expired_days_after')->unsigned()->nullable();
            $table->bigInteger('chat_id')->unsigned()->nullable();
            $table->foreign('chat_id')->references('id')->on('chats');
            $table->nullableMorphs('creator');
            $table->integer('quantity')->unsigned()->nullable();
            $table->jsonb('extra')->nullable();
            $table->timestamp('plan_started_at')->nullable();
            $table->timestamp('plan_expired_at')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('chat_payment_plans');
    }
}
