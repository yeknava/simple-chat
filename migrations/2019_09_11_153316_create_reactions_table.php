<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (config('simple-chat.use_reaction') === false) {
            return;
        }

        Schema::create('chat_message_reactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->tinyInteger('type')->unsigned();
            $table->bigInteger('member_id')->unsigned();
            $table->foreign('member_id')->references('id')->on('chat_members');
            $table->bigInteger('message_id')->unsigned();
            $table->foreign('message_id')->references('id')->on('chat_messages');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        if (config('simple-chat.use_reaction') === false) {
            return;
        }
        
        Schema::dropIfExists('chat_message_reactions');
    }
}
