<?php

namespace Yeknava\SimpleChat\Traits;

use Illuminate\Support\Facades\DB;
use Yeknava\SimpleChat\DTOs\MediaDTO;
use Yeknava\SimpleChat\Exceptions\AnonymousMessageIsNotPossibleException;
use Yeknava\SimpleChat\Exceptions\EmptyMessageException;
use Yeknava\SimpleChat\Exceptions\ExpiredMembershipException;
use Yeknava\SimpleChat\Exceptions\MessageMediaTypeException;
use Yeknava\SimpleChat\Exceptions\PermissionException;
use Yeknava\SimpleChat\Exceptions\SlowModeException;
use Yeknava\SimpleChat\Exceptions\UnspecifiedMessageMediaTypeException;
use Yeknava\SimpleChat\Exceptions\WrongChatException;
use Yeknava\SimpleChat\Models\Chat;
use Yeknava\SimpleChat\Models\ChatMedia;
use Yeknava\SimpleChat\Models\ChatMember;
use Yeknava\SimpleChat\Models\ChatMessage;
use Yeknava\SimpleChat\Models\ChatMessageTag;
use Yeknava\SimpleChat\Models\ChatTag;

trait HasMessage
{
    public function newMessage(
        ChatMember $chatMember = null,
        string $body = null,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        ChatMessage $replyTo = null,
        MediaDTO $mediaDTO = null
    ): ChatMessage {
        $messageTags = null;
        
        if (
            $this->permissions['send_media'] === false &&
            !empty($chatMember) &&
            $chatMember->role === ChatMember::ROLE_MEMBER &&
            !empty($mediaDTO) &&
            $mediaDTO->mediaType !== ChatMedia::TYPE_URL
        ) {
            throw new PermissionException();
        }

        if (
            $this->permissions['send_link'] === false &&
            !empty($chatMember) &&
            $chatMember->role === ChatMember::ROLE_MEMBER &&
            !empty($mediaDTO) &&
            $mediaDTO->mediaType === ChatMedia::TYPE_URL
        ) {
            throw new PermissionException();
        }

        $mediaPath = $mediaDTO->mediaPath ?? null;
        $mediaType = $mediaDTO->mediaType ?? null;
        $mediaOriginalFileName = $mediaDTO->mediaOriginalFileName ?? null;
        $handleUpload = $mediaDTO->handleUpload ?? false;

        if ($anonymous && $this->type === Chat::TYPE_DIRECT) {
            throw new AnonymousMessageIsNotPossibleException();
        }

        if (empty($chatMember) && $type != ChatMessage::TYPE_SYSTEM_MESSAGE) {
            throw new PermissionException();
        } elseif (!empty($chatMember)) {
            // check chat member is belongs to this chat
            if ($chatMember->chat_id != $this->id) {
                throw new PermissionException();
            }

            // make sure chat member has admin role in case type of this chat is channel
            if (
                $chatMember->role == ChatMember::ROLE_MEMBER &&
                $this->type == Chat::TYPE_CHANNEL
            ) {
                throw new PermissionException();
            }

            if (
                $chatMember->role == ChatMember::ROLE_MEMBER &&
                $this->permissions['send_message'] === false
            ) {
                throw new PermissionException();
            }

            // check if membership is expired
            if (
                !empty($chatMember) &&
                !empty($chatMember->expired_at) &&
                $chatMember->expired_at < now()
            ) {
                throw new ExpiredMembershipException();
            }

            if (
                $chatMember->role == ChatMember::ROLE_MEMBER &&
                $this->permissions['slow_mode'] != 0
            ) {
                $cantMessage = $chatMember
                    ->messages()
                    ->where('created_at', '>=', now()->subMinutes($this->permissions['slow_mode']))
                    ->count();

                if ($cantMessage) {
                    throw new SlowModeException();
                }
            }
        }

        if (empty($body) && empty($media)) {
            throw new EmptyMessageException();
        }

        if (
            !empty($replyTo) &&
            (int)$this->id !== (int)$replyTo->chat_id
        ) {
            throw new WrongChatException();
        }

        if (!empty($mediaPath) && $mediaType == ChatMedia::TYPE_CHAT_PHOTO) {
            throw new MessageMediaTypeException();
        }

        DB::beginTransaction();

        $memberRole = $chatMember->role ?? ChatMember::ROLE_SYSTEM;

        $message = new ChatMessage([
            'body' => $body,
            'type' => $type,
            'member_role' => $memberRole,
            'url' => $mediaType === ChatMedia::TYPE_URL ? $mediaPath : null,
            'anonymous' => $anonymous
        ]);

        preg_match_all('/#(\w+)/', $message->body, $messageTags);

        if (
            $memberRole == ChatMember::ROLE_MEMBER &&
            $this->permissions['review_needed']
        ) {
            $message->published_at = null;
        } else {
            $message->published_at = now();
            $message->publishedBy()->associate($chatMember);
        }

        if (!empty($chatMember)) {
            $message->member()->associate($chatMember);
        }

        $message->chat()->associate($this);

        if (!empty($mediaPath) && $mediaType !== ChatMedia::TYPE_URL) {
            if (
                empty($mediaType) &&
                ($type == ChatMessage::TYPE_MESSAGE || $type == ChatMessage::TYPE_LOCATION)
            ) {
                throw new UnspecifiedMessageMediaTypeException();
            }

            $mediaType = $type;

            if ($handleUpload) {
                $media = ChatMedia::uploadAndMake($this, $mediaPath, $mediaType, $mediaOriginalFileName);
            } else {
                $media = ChatMedia::makeNew($this, $mediaPath, $mediaType, $mediaOriginalFileName);
            }

            $message->media()->associate($media);
        }

        if (!empty($replyTo)) {
            $message->replyTo()->associate($replyTo);
        }

        $message->save();

        if (
            !empty($messageTags) &&
            count($messageTags) >= 1 &&
            config('simple-chat.use_tag') === true
        ) {
            foreach ($messageTags[1] as $tag) {
                $chatTag = ChatTag::firstOrCreate([
                    'title' => $tag
                ]);

                $messageTag = new ChatMessageTag();
                $messageTag->message()->associate($message);
                $messageTag->tag()->associate($chatTag);
                $messageTag->save();
            }
        }

        DB::commit();

        return $message;
    }

    public function newMessageWithUpload(
        ChatMember $chatMember,
        MediaDTO $mediaDTO,
        string $body = null,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        ChatMessage $replyTo = null
    ): ChatMessage {
        $mediaDTO->handleUpload = true;
        
        return $this->newMessage(
            $chatMember,
            $body,
            $type,
            $anonymous,
            $replyTo,
            $mediaDTO,
        );
    }

    public function forwardMessage(
        ChatMember $chatMember = null,
        ChatMessage $source,
        ChatMessage $replyTo = null
    ): ChatMessage {
        if (
            !empty($chatMember) &&
            $chatMember->role == ChatMember::ROLE_MEMBER &&
            $this->permissions['send_message'] === false
        ) {
            throw new PermissionException();
        }

        $message = new ChatMessage([
            'body' => $source->body,
            'type' => $source->type,
            'member_role' => $chatMember->role,
            'url' => $source->url
        ]);

        $message->member()->associate($chatMember);
        $message->chat()->associate($this);

        if (!empty($source->media)) {
            $message->media()->associate($source->media);
        }

        if (!empty($replyTo)) {
            $message->replyTo()->associate($replyTo);
        }

        $message->save();

        return $message;
    }

    public function newSystemMessage(
        string $body = null,
        ChatMessage $replyTo = null,
        MediaDTO $mediaDTO = null
    ): ChatMessage {
        return $this->newMessage(
            null,
            $body,
            ChatMessage::TYPE_SYSTEM_MESSAGE,
            false,
            $replyTo,
            $mediaDTO
        );
    }

    public function deleteMessage(
        ChatMessage $message,
        ChatMember $chatMember = null,
        bool $isSystem = false
    ): bool {
        if (
            (!empty($chatMember) &&
                $chatMember->role === ChatMember::ROLE_MEMBER &&
                (int)$message->member_id !== (int)$chatMember->id) &&
            !$isSystem
        ) {
            throw new PermissionException();
        }

        // check if membership is expired
        if (
            !empty($chatMember) &&
            !empty($chatMember->expired_at) &&
            $chatMember->expired_at < now()
        ) {
            throw new ExpiredMembershipException();
        }

        $message->delete();

        return true;
    }
}