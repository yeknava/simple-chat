<?php

namespace Yeknava\SimpleChat;

use Yeknava\SimpleChat\Models\Chat;

trait ChatTopic {
    public function chats()
    {
        return $this->morphMany(Chat::class, 'reference');
    }

    public function newDirectChat(
        array $data,
        $creator = null
    ): Chat {
        return Chat::newDirect($data, $this, $creator);
    }

    public function newGroupChat(
        array $data,
        $creator = null
    ): Chat {
        return Chat::newGroup($data, $this, $creator);
    }

    public function newChannelChat(
        array $data,
        $creator = null
    ): Chat {
        return Chat::newChannel($data, $this, $creator);
    }
}