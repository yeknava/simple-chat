<?php

namespace Yeknava\SimpleChat;

use Yeknava\SimpleChat\DTOs\MediaDTO;
use Yeknava\SimpleChat\Models\Chat;
use Yeknava\SimpleChat\Models\ChatMember;
use Yeknava\SimpleChat\Models\ChatMessage;
use Yeknava\SimpleChat\Models\ChatMessageReaction;

trait CanChat
{
    public function chats()
    {
        return $this->morphToMany(Chat::class, 'user', 'chat_members');
    }

    public function newDirectChat(
        array $data,
        $reference = null
    ): Chat {
        return Chat::newDirect($data, $reference, $this);
    }

    public function newGroupChat(
        array $data,
        $reference = null
    ): Chat {
        return Chat::newGroup($data, $reference, $this);
    }

    public function newChannelChat(
        array $data,
        $reference = null
    ): Chat {
        return Chat::newChannel($data, $reference, $this);
    }

    public function findChatMember(Chat $chat): ChatMember
    {
        return $chat->members()
            ->where('user_id', $this->getKey())
            ->where('user_type', $this->getMorphClass())
            ->first();
    }

    public function newMessage(
        string $body = null,
        Chat $chat,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        ChatMessage $replyTo = null,
        MediaDTO $mediaDTO = null
    ): ChatMessage {
        return $chat->newMessage(
            $this->findChatMember($chat),
            $body,
            $type,
            $anonymous,
            $replyTo,
            $mediaDTO
        );
    }

    public function newMessageWithUpload(
        MediaDTO $mediaDTO,
        string $body = null,
        Chat $chat,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        ChatMessage $replyTo = null
    ): ChatMessage {
        return $chat->newMessageWithUpload(
            $this->findChatMember($chat),
            $mediaDTO,
            $body,
            $type,
            $anonymous,
            $replyTo
        );
    }

    public function forwardMessage(
        ChatMessage $source,
        Chat $chat,
        ChatMessage $replyTo = null
    ): ChatMessage {
        return $chat->forwardMessage(
            $this->findChatMember($chat),
            $source,
            $replyTo
        );
    }

    public function reactToMessage(
        int $type,
        ChatMessage $message
    ) : ChatMessageReaction
    {
        $chatMember = $this->findChatMember($message->chat);

        return $message->react($chatMember, $type);
    }
}
