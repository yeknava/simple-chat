<?php

namespace Yeknava\SimpleChat\DTOs;

class MediaDTO {
    
    public ?string $mediaPath;

    public ?string $mediaType;

    public ?string $mediaOriginalFileName;

    public ?bool $handleUpload;

    public function __construct(
        ?string $mediaPath,
        ?string $mediaType,
        ?string $mediaOriginalFileName = null,
        ?bool $handleUpload = null
    ) {
        $this->mediaPath = $mediaPath;
        $this->mediaType = $mediaType;
        $this->mediaOriginalFileName = $mediaOriginalFileName;
        $this->handleUpload = $handleUpload;
    }
}