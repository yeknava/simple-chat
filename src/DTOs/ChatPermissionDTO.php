<?php

namespace Yeknava\SimpleChat\DTOs;

class ChatPermissionDTO {
    public bool $sendMessage = true;
    public bool $sendMedia = true;
    public bool $sendLink = true;
    public bool $addMember = true;
    public bool $changeChatInfo = true;
    public bool $reviewNeeded = false;
    public bool $pinMessage = true;
    public bool $allowSpectators = true;
    public int $slowMode = 0; //minutes
    public ?int $membershipLimit = null;

    public function toArray() :array {
        return [
            'send_message' => $this->sendMessage,
            'send_media' => $this->sendMedia,
            'send_link' => $this->sendLink,
            'add_member' => $this->addMember,
            'change_chat_info' => $this->changeChatInfo,
            'review_needed' => $this->reviewNeeded,
            'pin_message' => $this->pinMessage,
            'allow_spectators' => $this->allowSpectators,
            'slow_mode' => $this->slowMode,
            'membership_limit' => $this->membershipLimit
        ];
    }

    public function toJson() :string {
        return json_encode($this->toArray());
    }
}