<?php

namespace Yeknava\SimpleChat;

use Illuminate\Database\Eloquent\Collection;
use Yeknava\SimpleChat\Models\Chat;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Yeknava\SimpleChat\DTOs\MediaDTO;
use Yeknava\SimpleChat\Exceptions\DisabledFeatureException;
use Yeknava\SimpleChat\Models\ChatMedia;
use Yeknava\SimpleChat\Models\ChatMember;
use Yeknava\SimpleChat\Models\ChatMessage;
use Yeknava\SimpleChat\Models\ChatPaymentPlan;
use Yeknava\SimpleChat\Exceptions\EmptyChatValueException;
use Yeknava\SimpleChat\Exceptions\InvalidDataException;
use Yeknava\SimpleChat\Exceptions\UnspecifiedUserException;
use Yeknava\SimpleChat\Models\ChatMessageReaction;
use Yeknava\SimpleChat\Models\ChatTotalReaction;

class SimpleChat
{
    protected ?Chat $chat;
    protected $user;
    protected ?ChatMember $chatMember = null;

    public function __construct(Chat $chat = null)
    {
        $this->chat = $chat;
    }

    public function getChat(): ?Chat
    {
        return $this->chat;
    }

    public function setChat(Chat $chat): self
    {
        $this->chat = $chat;

        return $this;
    }

    public function setChatById(int $chatId): self
    {
        return $this->setChat(Chat::findOrFail($chatId));
    }

    public function getUser()
    {
        return $this->user;
    }

    public function setUser($user)
    {
        $this->user = $user;

        return $this;
    }

    private function checkIfChatExists()
    {
        if (empty($this->chat) || !$this->chat instanceof Chat) {
            throw new EmptyChatValueException();
        }
    }

    public function newDirectChat(
        array $data,
        $reference = null
    ): Chat {
        $chat = Chat::newDirect($data, $reference, $this->user);
        $this->chat = $chat;

        return $chat;
    }

    public function newGroupChat(
        array $data,
        $reference = null
    ): Chat {
        $chat = Chat::newGroup($data, $reference, $this->user);
        $this->chat = $chat;

        return $chat;
    }

    public function newChannelChat(
        array $data,
        $reference = null
    ): Chat {
        $chat = Chat::newChannel($data, $reference, $this->user);
        $this->chat = $chat;

        return $chat;
    }

    public function findChatMember(Chat $chat = null, bool $force = false): ?ChatMember
    {
        $chat = $chat ?? $this->chat;

        if (empty($this->user)) {
            throw new UnspecifiedUserException();
        }

        if (
            !$force &&
            !is_null($this->chatMember) &&
            (int)$this->user->id === (int)$this->chatMember->user_id
        ) {
            return $this->chatMember;
        }

        $this->chatMember = $chat->members()
            ->where('user_id', $this->user->getKey())
            ->where('user_type', $this->user->getMorphClass())
            ->first();

        return $this->chatMember;
    }

    public function findChatMemberById(
        int $chatId,
        $userId,
        string $userMorphClass
    ): ?ChatMember {
        $chat = Chat::findOrFail($chatId);

        return $chat->members()
            ->where('user_id', $userId)
            ->where('user_type', $userMorphClass)
            ->first();
    }

    public function addMember(
        Model $user,
        string $role = ChatMember::ROLE_MEMBER,
        ChatMember $invitedBy = null,
        ChatPaymentPlan $plan = null
    ): ChatMember {
        return $this->chat->addMember(
            $user,
            $role,
            $invitedBy,
            $plan
        );
    }

    public function addMemberById(
        $userId,
        string $userMorphClass,
        string $role = ChatMember::ROLE_MEMBER,
        int $invitedById = null,
        int $planId = null
    ): ChatMember {
        return $this->chat->addMemberById(
            $userId,
            $userMorphClass,
            $role,
            $invitedById,
            $planId
        );
    }

    public function newMessage(
        string $body = null,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        ChatMessage $replyTo = null,
        MediaDTO $mediaDTO = null
    ): ChatMessage {
        $this->checkIfChatExists();

        return $this->chat->newMessage(
            $this->findChatMember($this->chat),
            $body,
            $type,
            $anonymous,
            $replyTo,
            $mediaDTO
        );
    }

    public function newMessageById(
        string $body = null,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        int $replyToId = null,
        MediaDTO $mediaDTO = null
    ): ChatMessage {
        $this->checkIfChatExists();

        return $this->chat->newMessage(
            $this->findChatMember($this->chat),
            $body,
            $type,
            $anonymous,
            $replyToId ? ChatMessage::findOrFail($replyToId) : null,
            $mediaDTO
        );
    }

    public function newSystemMessage(
        string $body = null,
        ChatMessage $replyTo = null,
        MediaDTO $mediaDTO = null
    ): ChatMessage {
        $this->checkIfChatExists();

        return $this->chat->newSystemMessage(
            $body,
            $replyTo,
            $mediaDTO
        );
    }


    public function newSystemMessageById(
        string $body = null,
        int $replyToId = null,
        MediaDTO $mediaDTO = null
    ): ChatMessage {
        return $this->newSystemMessage(
            $body,
            $replyToId ? ChatMessage::findOrFail($replyToId) : null,
            $mediaDTO
        );
    }

    public function newMessageWithUpload(
        MediaDTO $mediaDTO,
        string $body = null,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        ChatMessage $replyTo = null
    ): ChatMessage {
        $this->checkIfChatExists();

        return $this->chat->newMessageWithUpload(
            $this->findChatMember($this->chat),
            $mediaDTO,
            $body,
            $type,
            $anonymous,
            $replyTo
        );
    }

    public function newMessageWithUploadById(
        MediaDTO $mediaDTO,
        string $body = null,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        int $replyToId = null
    ): ChatMessage {
        return $this->newMessageWithUpload(
            $mediaDTO,
            $body,
            $type,
            $anonymous,
            $replyToId ? ChatMessage::findOrFail($replyToId) : null
        );
    }

    public function forwardMessage(
        ChatMessage $source,
        ChatMessage $replyTo = null
    ): ChatMessage {
        $this->checkIfChatExists();

        return $this->chat->forwardMessage(
            $this->findChatMember($this->chat),
            $source,
            $replyTo
        );
    }

    public function forwardMessageById(
        int $sourceId,
        int $replyToId = null
    ): ChatMessage {
        return $this->forwardMessage(
            ChatMessage::findOrFail($sourceId),
            $replyToId ? ChatMessage::findOrFail($replyToId) : null
        );
    }

    public function newPaymentPlan(array $data, Model $creator = null): ChatPaymentPlan
    {
        $this->checkIfChatExists();

        if (
            !empty($s = $data['plan_started_at']) &&
            !empty($e = $data['plan_expired_at']) &&
            (new Carbon($s))->toTimeString() > (new Carbon($e))->toTimeString()
        ) {
            throw new InvalidDataException();
        }

        $paymentPlan = new ChatPaymentPlan($data);
        $paymentPlan->chat()->associate($this->chat);

        if (!empty($creator)) $paymentPlan->creator()->associate($creator);

        $paymentPlan->save();

        return $paymentPlan;
    }

    public function newChatPhoto(
        string $path,
        string $originalFileName = null,
        string $driver = null,
        bool $deleteOthers = false
    ): ChatMedia {
        $this->checkIfChatExists();

        $media = $this->chat->newChatPhoto(
            $path,
            $originalFileName,
            $driver,
            $deleteOthers
        );

        return $media;
    }

    public function newUploadChatPhoto(
        string $tempFilePath,
        string $originalFileName = null,
        string $driver = null,
        bool $deleteOthers = false
    ): ChatMedia {
        $this->checkIfChatExists();

        $media = $this->chat->newUploadChatPhoto(
            $tempFilePath,
            $originalFileName,
            $driver,
            $deleteOthers
        );

        return $media;
    }

    public function isMember(ChatMember $chatMember = null): bool {
        $this->checkIfChatExists();

        if (!empty($chatMember)) {
            return (int)$chatMember->chat_id === (int)$this->chat->id;
        }

        return is_null($this->findChatMember()) ? false : true;   
    }

    public function isMemberById(int $chatMemberId): bool {
        return $this->isMember(
            ChatMember::findOrFail($chatMemberId)
        );
    }

    public function hasAccessToChat() : bool {
        $this->checkIfChatExists();
        
        return $this->chat->hasAccessToChat($this->user);
    }

    public function canView(): bool {
        $this->checkIfChatExists();
        
        return $this->chat->canView($this->user);
    }

    public function deleteMessage(
        ChatMessage $message,
        ChatMember $chatMember = null,
        bool $isSystem = false
    ): bool {
        $this->checkIfChatExists();
        
        return $this->chat->deleteMessage(
            $message,
            $chatMember,
            $isSystem
        );
    }

    public function deleteMessageById(
        int $messageId,
        int $chatMemberId = null,
        bool $isSystem = false
    ): bool {        
        return $this->deleteMessage(
            ChatMessage::findOrFail($messageId),
            $chatMemberId ? ChatMember::findOrFail($chatMemberId) : null,
            $isSystem
        );
    }

    public function deleteChat(
        ChatMember $chatMember = null,
        bool $isSystem = false,
        bool $deleteMessages = true
    ): bool {
        $this->checkIfChatExists();

        return $this->chat->deleteChat(
            $chatMember,
            $isSystem,
            $deleteMessages
        );
    }

    public function deleteChatById(
        int $memberId = null,
        bool $isSystem = false,
        bool $deleteMessages = true
    ): bool {
        return $this->deleteChat(
            ChatMember::findOrFail($memberId),
            $isSystem,
            $deleteMessages
        );
    }

    public function publish(
        ChatMessage $message,
        ChatMember $admin
    ): ChatMessage
    {
        return $message->publish($admin);
    }

    public function publishById(
        int $messageId,
        int $adminId
    ): ChatMessage
    {
        return $this->publish(
            ChatMessage::findOrFail($messageId),
            ChatMember::findOrFail($adminId)
        );
    }

    public function react(
        int $type,
        ChatMessage $message,
        ChatMember $chatMember = null
    ): ChatMessageReaction
    {
        if (config('simple-chat.use_reaction') !== true) {
            throw new DisabledFeatureException();
        }
        
        if (empty($chatMember)) {
            $chatMember = $this->findChatMember($this->user);
        }
        
        return $message->react($chatMember, $type);
    }

    public function reactById(
        int $type,
        int $messageId,
        int $memberId = null
    ): ChatMessageReaction
    {
        $member = null;
        $message = ChatMessage::findOrFail($messageId);
        
        if (empty($chatMember)) {
            $member = ChatMember::findOrFail($memberId);
        }
        
        return $this->react(
            $type,
            $message,
            $member
        );
    }

    public function totalReactions(
        ChatMessage $message,
        int $type = null
    ): Collection {
        if (config('simple-chat.use_reaction') !== true) {
            throw new DisabledFeatureException();
        }

        $totalReaction = $message->totalReactions();

        if (!empty($type)) {
            $totalReaction->where('type', $type);
        }

        return $totalReaction->get();
    }

    public function totalReactionsById(
        int $messageId,
        int $type = null
    ): Collection {
        if (config('simple-chat.use_reaction') !== true) {
            throw new DisabledFeatureException();
        }

        $message = ChatMessage::findOrFail($messageId);
        
        return $this->totalReactions($message, $type);
    }

    public function messagesTotalReactions(
        array $messageIds,
        int $chatMemberId = null,
        bool $toArray = false
    ): array {
        if (config('simple-chat.use_reaction') !== true) {
            throw new DisabledFeatureException();
        }

        $totalReactions = ChatTotalReaction::whereIn('message_id', $messageIds)
            ->select('total', 'message_id', 'type')
            ->get();
        $totalReplies = ChatMessage::whereIn('reply_to_message_id', $messageIds)
            ->selectRaw('reply_to_message_id as message_id, count(*) as replies')
            ->groupBy('reply_to_message_id')
            ->get();
        $memberReactions = [];

        if (!empty($chatMemberId)) {
            $memberReactions = ChatMessageReaction::whereIn('message_id', $messageIds)
                ->where('member_id', $chatMemberId)
                ->select('message_id', 'member_id', 'type')
                //->groupBy('message_id', 'type', 'member_id')
                ->get();
        }

        return [
            'total_reactions' => $toArray ? $totalReactions->toArray() : $totalReactions,
            'total_replies' => $toArray ? $totalReplies->toArray() : $totalReplies,
            'member_reactions' => $toArray ? $memberReactions->toArray() : $memberReactions,
        ];
    }

    public function pinMessage(
        int $messageId,
        ChatMember $member = null
    ) :ChatMessage {
        $member ??= $this->findChatMember();
        $message = ChatMessage::findOrFail($messageId);

        return $message->pin($member);
    }

    public function pinMessageById(
        int $messageId,
        int $memberId = null
    ) :ChatMessage {
        $member = null;

        if (!empty($memberId)) {
            $member = ChatMember::findOrFail($memberId);
        }

        return $this->pinMessage($messageId, $member);
    }

    public function editMessage(
        int $messageId,
        ChatMember $member = null,
        string $body = null,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        MediaDTO $mediaDTO = null
    ) :ChatMessage {
        $member ??= $this->findChatMember();
        $message = ChatMessage::findOrFail($messageId);

        return $message->edit(
            $member,
            $body,
            $type,
            $anonymous,
            $mediaDTO
        );
    }

    public function editMessageById(
        int $messageId,
        int $memberId = null,
        string $body = null,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        MediaDTO $mediaDTO = null
    ) :ChatMessage {
        $member = null;

        if (!empty($memberId)) {
            $member = ChatMember::findOrFail($memberId);
        }

        return $this->editMessageById(
            $messageId,
            $member,
            $body,
            $type,
            $anonymous,
            $mediaDTO
        );
    }
}
