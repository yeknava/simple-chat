<?php

namespace Yeknava\SimpleChat\Exceptions;

class ExpiredMembershipException extends SimpleChatException {
}
