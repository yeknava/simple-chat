<?php

namespace Yeknava\SimpleChat\Exceptions;

class UnspecifiedMessageMediaTypeException extends SimpleChatException {
}
