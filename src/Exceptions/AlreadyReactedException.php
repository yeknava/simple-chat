<?php

namespace Yeknava\SimpleChat\Exceptions;

use Yeknava\SimpleChat\Models\ChatMessageReaction;

class AlreadyReactedException extends SimpleChatException {
    private ChatMessageReaction $reaction;

    public function __construct(ChatMessageReaction $reaction)
    {
        $this->reaction = $reaction;
    }

    public function getReaction()
    {
        return $this->reaction;
    }
}
