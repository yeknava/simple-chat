<?php

namespace Yeknava\SimpleChat\Exceptions;

class DisabledFeatureException extends SimpleChatException {
}
