<?php

namespace Yeknava\SimpleChat\Exceptions;

class AnonymousMessageIsNotPossibleException extends SimpleChatException {
}
