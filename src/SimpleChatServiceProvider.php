<?php

namespace Yeknava\SimpleChat;

use Illuminate\Support\ServiceProvider;

class SimpleChatServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/simple-chat.php' => $this->app->configPath().'/simple-chat.php',

            dirname(__DIR__, 1) . '/migrations/' =>
                database_path('migrations'),
        ]);
    }
}
