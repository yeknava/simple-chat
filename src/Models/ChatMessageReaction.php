<?php

namespace Yeknava\SimpleChat\Models;

use Illuminate\Database\Eloquent\Model;

class ChatMessageReaction extends Model
{
    public const BEHAVIOUR_ONCE = 'once';
    public const BEHAVIOUR_PER_TYPE = 'per_type';
    public const BEHAVIOUR_INFINITE = 'infinite';

    protected static function booted()
    {
        static::saved(function ($reaction) {
            $totalReaction = ChatTotalReaction::where('message_id', $reaction->message_id)
                ->where('type', $reaction->type)
                ->first();
            
            if (empty($totalReaction)) {
                $totalReaction = new ChatTotalReaction();
                $totalReaction->type = $reaction->type;
                $totalReaction->total = 1;
                $totalReaction->message()->associate($reaction->message);
            } else {
                $totalReaction->total = $totalReaction->total + 1;
            }

            $totalReaction->save();
        });

        static::deleted(function ($reaction) {
            $totalReaction = ChatTotalReaction::where('message_id', $reaction->message_id)
                ->where('type', $reaction->type)
                ->first();
            $totalReaction->total = $totalReaction->total - 1;
            $totalReaction->save();
        });
    }

    protected $fillable = [
        'type'
    ];

    protected $casts = [
    ];

    public function message()
    {
        return $this->belongsTo(ChatMessage::class);
    }

    public function member()
    {
        return $this->belongsTo(ChatMember::class);
    }
}
