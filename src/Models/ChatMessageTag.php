<?php

namespace Yeknava\SimpleChat\Models;

use Illuminate\Database\Eloquent\Relations\Pivot;

class ChatMessageTag extends Pivot
{
    public function message()
    {
        return $this->belongsTo(ChatMessage::class);
    }

    public function tag()
    {
        return $this->belongsTo(ChatTag::class);
    }
}
