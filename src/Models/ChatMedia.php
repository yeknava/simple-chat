<?php

namespace Yeknava\SimpleChat\Models;

use Throwable;
use Illuminate\Support\Facades\File;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yeknava\SimpleChat\Exceptions\FileNotFoundException;

class ChatMedia extends Model
{
    use SoftDeletes;

    const TYPE_CHAT_PHOTO = 'chat_photo';
    const TYPE_VIDEO = 'video';
    const TYPE_PHOTO = 'photo';
    const TYPE_VOICE = 'voice';
    const TYPE_AUDIO = 'audio';
    const TYPE_FILE = 'file';
    
    const TYPE_URL = 'url';
    
    protected $fillable = [
        'type', 'driver', 'filename',
        'extension', 'mimetype', 'path',
        'sha1', 'filesize', 'uploader_ip'
    ];

    protected $casts = [
        'extra' => 'array',
    ];

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }

    public function user()
    {
        return $this->morphTo();
    }

    public static function makeNew(
        Chat $chat,
        string $path,
        string $type,
        string $originalFileName = null,
        string $driver = null
    ): ChatMedia {        
        $media = new ChatMedia([
            'type' => $type,
            'path' => $path,
            'filename' => $originalFileName,
            'mimetype' => mime_content_type($path) ?? null,
            'driver' => $driver ?? config('simple-chat.media_storage_driver'),
            'extension' => pathinfo($path, PATHINFO_EXTENSION),
            'sha1' => sha1_file($path),
            'filesize' => filesize($path),
            'uploader_ip' => request()->ip() ?? null
        ]);
        $media->chat()->associate($chat);
        $media->save();

        return $media;
    }

    public static function uploadAndMake(
        Chat $chat,
        string $tempFilePath,
        string $type,
        string $originalFileName = null,
        string $driver = null
    ): ChatMedia {
        if (!is_file($tempFilePath)) {
            throw new FileNotFoundException();
        }

        $path = config('simple-chat.media_default_path');

        $driver = $driver ?? config('simple-chat.media_storage_disk');

        if ($type == static::TYPE_CHAT_PHOTO) {
            $path .= '/chat-photos';
        } else {
            $path .= '/messages';
        }

        // check if file is base64
        if (strpos($tempFilePath, ';base64,') !== false) {
            [, $tempFilePath] = $tmpType = explode(';', $tempFilePath);
            [, $tempFilePath] = explode(',', $tempFilePath);
            $mimeType = ltrim($tmpType[0], 'data:');

            $binaryData = base64_decode($tempFilePath);
            $file = tempnam(sys_get_temp_dir(), 'b64');
            file_put_contents($file, $binaryData);
            
            $tempFilePath = $file;
        }

        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            $path = rtrim($path, '\\');
            $path .= '\\' . uniqid() . mt_rand(1, 99999) . '.' . pathinfo($tempFilePath, PATHINFO_EXTENSION);
        } else {
            $path = rtrim($path, '/');
            $path .= '/' . uniqid() . mt_rand(1, 99999) . '.' . pathinfo($tempFilePath, PATHINFO_EXTENSION);
        }

        try {
            Storage::disk($driver)->put($path, File::get($tempFilePath));

            return static::makeNew(
                $chat,
                Storage::disk($driver)->path($path),
                $type,
                $originalFileName,
                $driver
            );
        } catch (Throwable $e) {
            if (file_exists($path)) {
                Storage::delete($path);
            }

            throw $e;
        }
    }
}
