<?php

namespace Yeknava\SimpleChat\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ChatPaymentPlan extends Model
{
    use SoftDeletes;
    
    protected $fillable = [
        'title', 'currency', 'price',
        'plan_started_at', 'plan_expired_at', 'membership_role',
        'extra',  'membership_expired_days_after', 'quantity'
    ];

    protected $casts = [
        'plan_started_at' => 'datetime',
        'plan_expired_at'  => 'datetime',
        'extra' => 'array',
    ];

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }

    public function creator()
    {
        return $this->morphTo();
    }
}
