<?php

namespace Yeknava\SimpleChat\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yeknava\SimpleChat\DTOs\MediaDTO;

class ChatMember extends Model
{
    use SoftDeletes;
    
    const ROLE_MEMBER = 'member';
    const ROLE_CHAT_ADMIN = 'chat_admin';
    const ROLE_OWNER = 'owner';
    const ROLE_APP_ADMIN = 'app_admin';
    const ROLE_SYSTEM = 'system';

    protected $fillable = [
        'role'
    ];

    protected $casts = [
        'expired_at' => 'datetime',
        'extra' => 'array',
    ];

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }

    public function user()
    {
        return $this->morphTo();
    }

    public function plan()
    {
        return $this->belongsTo(ChatPaymentPlan::class);
    }

    public function invitedBy()
    {
        return $this->belongsTo(static::class, 'invited_by');
    }

    public function messages()
    {
        return $this->hasMany(ChatMessage::class);
    }

    public function message(
        string $body = null,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        ChatMessage $replyTo = null,
        MediaDTO $mediaDTO = null
    ) : ChatMessage
    {
        return $this->chat->message(
            $this,
            $body,
            $type,
            $anonymous,
            $replyTo,
            $mediaDTO
        );
    }
}
