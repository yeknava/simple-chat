<?php

namespace Yeknava\SimpleChat\Models;

use Illuminate\Database\Eloquent\Model;

class ChatTotalReaction extends Model
{
    public $timestamps = false;

    protected $fillable = [
    ];

    protected $casts = [
    ];

    public function message()
    {
        return $this->belongsTo(ChatMessage::class);
    }
}
