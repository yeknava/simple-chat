<?php

namespace Yeknava\SimpleChat\Models;

use Illuminate\Database\Eloquent\Model;

class ChatTag extends Model
{
    protected $fillable = [
        'title'
    ];

    protected $casts = [
    ];
}
