<?php

namespace Yeknava\SimpleChat\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Facades\DB;
use Yeknava\SimpleChat\DTOs\MediaDTO;
use Yeknava\SimpleChat\Exceptions\AlreadyPublishedException;
use Yeknava\SimpleChat\Exceptions\AlreadyReactedException;
use Yeknava\SimpleChat\Exceptions\AnonymousMessageIsNotPossibleException;
use Yeknava\SimpleChat\Exceptions\DisabledFeatureException;
use Yeknava\SimpleChat\Exceptions\EmptyMessageException;
use Yeknava\SimpleChat\Exceptions\ExpiredMembershipException;
use Yeknava\SimpleChat\Exceptions\MessageMediaTypeException;
use Yeknava\SimpleChat\Exceptions\OwnerReactionException;
use Yeknava\SimpleChat\Exceptions\PermissionException;
use Yeknava\SimpleChat\Exceptions\UnspecifiedMessageMediaTypeException;
use Yeknava\SimpleChat\Exceptions\WrongChatException;

class ChatMessage extends Model
{
    use SoftDeletes;

    const TYPE_SYSTEM_MESSAGE = 'system_message';
    const TYPE_MESSAGE = 'message';
    const TYPE_LOCATION = 'location';
    const TYPE_VIDEO = 'video';
    const TYPE_PHOTO = 'photo';
    const TYPE_AUDIO = 'audio';
    const TYPE_VOICE = 'voice';
    const TYPE_FILE = 'file';

    protected static function booted()
    {
        static::addGlobalScope('published', function (Builder $builder) {
            $builder->whereNotNull('published_at');
        });
    }

    protected $fillable = [
        'body', 'url', 'type', 'anonymous'
    ];

    protected $casts = [
        'extra' => 'array',
        'published_at' => 'datetime',
    ];

    public function replyTo()
    {
        return $this->belongsTo(static::class, 'reply_to_message_id');
    }

    public function chat()
    {
        return $this->belongsTo(Chat::class);
    }

    public function member()
    {
        return $this->belongsTo(ChatMember::class);
    }

    public function publishedBy()
    {
        return $this->belongsTo(ChatMember::class, 'published_by');
    }

    public function media()
    {
        return $this->belongsTo(ChatMedia::class);
    }

    public function replies() {
        return $this->hasMany(static::class, 'reply_to_message_id');
    }

    public function tags() {
        return $this->belongsToMany(
            ChatTag::class,
            (new ChatMessageTag)->getTable(),
            'message_id',
            'tag_id'
        );
    }

    public function totalReactions()
    {
        return $this->hasMany(ChatMessageReaction::class);
    }

    public function getMemberIdAttribute($value)
    {
        return $this->anonymous == true ? null : $value;
    }

    public function edit(
        ChatMember $chatMember = null,
        string $body = null,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        MediaDTO $mediaDTO = null
    ) :ChatMessage {
        $messageTags = null;
        
        if (
            $this->chat->permissions['send_media'] === false &&
            !empty($chatMember) &&
            $chatMember->role === ChatMember::ROLE_MEMBER &&
            !empty($mediaDTO) &&
            $mediaDTO->mediaType !== ChatMedia::TYPE_URL
        ) {
            throw new PermissionException();
        }

        if (
            $this->chat->permissions['send_link'] === false &&
            !empty($chatMember) &&
            $chatMember->role === ChatMember::ROLE_MEMBER &&
            !empty($mediaDTO) &&
            $mediaDTO->mediaType === ChatMedia::TYPE_URL
        ) {
            throw new PermissionException();
        }
        
        $this->type = $type;
        $this->anonymous = $anonymous;
        
        $mediaPath = $mediaDTO->mediaPath ?? null;
        $mediaType = $mediaDTO->mediaType ?? null;
        $mediaOriginalFileName = $mediaDTO->mediaOriginalFileName ?? null;
        $handleUpload = $mediaDTO->handleUpload ?? false;
        $chatMember ??= $this->member;

        if ($anonymous && $this->type === Chat::TYPE_DIRECT) {
            throw new AnonymousMessageIsNotPossibleException();
        }

        if (empty($chatMember) && $type != ChatMessage::TYPE_SYSTEM_MESSAGE) {
            throw new PermissionException();
        } elseif (!empty($chatMember)) {
            // check if membership is expired
            if (
                !empty($chatMember) &&
                !empty($chatMember->expired_at) &&
                $chatMember->expired_at < now()
            ) {
                throw new ExpiredMembershipException();
            } elseif (
                $chatMember->id != $this->member->id &&
                $chatMember->role === ChatMember::ROLE_MEMBER
            ) {
                throw new PermissionException();
            }
        }

        if (!empty($mediaPath) && $mediaType == ChatMedia::TYPE_CHAT_PHOTO) {
            throw new MessageMediaTypeException();
        }

        DB::beginTransaction();

        if (!empty($body) && $body !== $this->body && config('simple-chat.use_tag') === true) {
            preg_match_all('/#(\w+)/', $body, $messageTags);
    
            if (
                $chatMember->role == ChatMember::ROLE_MEMBER &&
                $this->permissions['review_needed']
            ) {
                $this->published_at = null;
            }
        }

        $this->body = $body;

        if (!empty($mediaPath) && $mediaType !== ChatMedia::TYPE_URL) {
            if (
                empty($mediaType) &&
                ($type == ChatMessage::TYPE_MESSAGE || $type == ChatMessage::TYPE_LOCATION)
            ) {
                throw new UnspecifiedMessageMediaTypeException();
            }

            $mediaType = $type;

            if ($handleUpload) {
                $media = ChatMedia::uploadAndMake($this->chat, $mediaPath, $mediaType, $mediaOriginalFileName);
            } else {
                $media = ChatMedia::makeNew($this->chat, $mediaPath, $mediaType, $mediaOriginalFileName);
            }

            $this->media()->associate($media);
        } elseif ($mediaType === ChatMedia::TYPE_URL) {
            $this->url = $mediaPath;
        } elseif (
            empty($mediaPath) &&
            $mediaType !== ChatMedia::TYPE_URL &&
            !empty($this->media)
        ) {
            $this->media()->disassociate();
        }

        $this->save();

        if (empty($body) && config('simple-chat.use_tag') === true) {
            foreach($this->tags as $tag) {
                $tag->pivot->delete();
            }
        } elseif (
            !empty($messageTags) &&
            count($messageTags) >= 1 &&
            config('simple-chat.use_tag') === true
        ) {
            foreach ($messageTags[1] as $tag) {
                $chatTag = ChatTag::firstOrCreate([
                    'title' => $tag
                ]);

                $messageTag = new ChatMessageTag();
                $messageTag->message()->associate($this);
                $messageTag->tag()->associate($chatTag);
                $messageTag->save();
            }
        }

        DB::commit();

        return $this;
    }

    public function reply(
        ChatMember $chatMember = null,
        string $body = null,
        string $type = ChatMessage::TYPE_MESSAGE,
        bool $anonymous = false,
        MediaDTO $mediaDTO = null
    ) :ChatMessage {
        return $this->chat->newMessage(
            $chatMember,
            $body,
            $type,
            $anonymous,
            $this,
            $mediaDTO
        );
    }

    public function totalReplies() :int {
        return ChatMessage::where('reply_to_message_id', $this->id)->count();
    }

    public function deleteMessage(
        ChatMember $chatMember,
        bool $isSystem = false
    ) :bool {
        return $this->chat->deleteMessage(
            $chatMember,
            $this,
            $isSystem
        );
    }

    public function react(
        ChatMember $member,
        int $type
    ) :ChatMessageReaction {
        if (config('simple-chat.use_reaction', true) === false) {
            throw new DisabledFeatureException();
        }

        $selfReaction = config('simple-chat.react_to_self_messages', false);

        if ($selfReaction === false && (int)$member->id === (int)$this->member_id) {
            throw new OwnerReactionException();
        }

        $reactionConfig = config('simple-chat.reaction_behavior', ChatMessageReaction::BEHAVIOUR_PER_TYPE);
        $reactions = ChatMessageReaction::where('member_id', $member->id)->where('message_id', $this->id);
        $reaction = null;

        if ($reactionConfig === ChatMessageReaction::BEHAVIOUR_ONCE) {
            $reaction = $reactions->first();
        } elseif ($reactionConfig === ChatMessageReaction::BEHAVIOUR_PER_TYPE) {
            $reaction = $reactions->where('type', $type)->first();
        }

        if (!empty($reaction)) {
            throw new AlreadyReactedException($reaction);
        }

        $reaction = new ChatMessageReaction();
        $reaction->type = $type;
        $reaction->member()->associate($member);
        $reaction->message()->associate($this);
        $reaction->save();

        return $reaction;
    }

    public function publish(
        ChatMember $admin
    ): ChatMessage {
        if ($admin->role === ChatMember::ROLE_MEMBER) {
            throw new PermissionException();
        }

        if (!empty($this->published_at)) {
            throw new AlreadyPublishedException();
        }

        $this->published_at = now();
        $this->publishedBy()->associate($admin);
        $this->save();

        return $this;
    }

    public function pin(
        ChatMember $member
    ): ChatMessage {
        if (
            $this->chat->permissions['pin_message'] === false &&
            $member->role === ChatMember::ROLE_MEMBER
        ) {
            throw new PermissionException();
        }

        $this->pinned = true;
        $this->save();

        return $this;
    }
}
