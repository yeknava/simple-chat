<?php

namespace Yeknava\SimpleChat\Models;

use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Yeknava\SimpleChat\DTOs\ChatPermissionDTO;
use Yeknava\SimpleChat\Exceptions\WrongChatException;
use Yeknava\SimpleChat\Exceptions\PermissionException;
use Yeknava\SimpleChat\Exceptions\ExpiredPlanException;
use Yeknava\SimpleChat\Exceptions\NotStartedPlanException;
use Yeknava\SimpleChat\Exceptions\MembershipLimitException;
use Yeknava\SimpleChat\Exceptions\ExpiredMembershipException;
use Yeknava\SimpleChat\Exceptions\PlanQuantityLimitException;
use Yeknava\SimpleChat\Traits\HasMessage;

class Chat extends Model
{
    use SoftDeletes, HasMessage;

    const TYPE_DIRECT = 'direct';
    const TYPE_GROUP = 'group';
    const TYPE_CHANNEL = 'channel';

    protected $fillable = [
        'title', 'description', 'creator_role',
        'category', 'type', 'permissions',
        'started_at', 'closed_at', 'extra',
    ];

    protected $casts = [
        'started_at' => 'datetime',
        'closed_at'  => 'datetime',
        'permissions' => 'array',
        'extra' => 'array',
    ];

    public function creator()
    {
        return $this->morphTo();
    }

    public function reference()
    {
        return $this->morphTo();
    }

    public function photo()
    {
        return $this->belongsTo(ChatMedia::class);
    }

    public function members()
    {
        return $this->hasMany(ChatMember::class);
    }

    public function admins()
    {
        return $this->hasMany(ChatMember::class)->where('role', ChatMember::ROLE_CHAT_ADMIN);
    }

    public function owner()
    {
        return $this->hasOne(ChatMember::class)->where('role', ChatMember::ROLE_OWNER);
    }

    public function messages()
    {
        return $this->hasMany(ChatMessage::class);
    }

    public function allMessages()
    {
        return $this->hasMany(ChatMessage::class)->withoutGlobalScope();
    }

    public function photos()
    {
        return $this->hasMany(ChatMedia::class)->where('type', ChatMedia::TYPE_CHAT_PHOTO);
    }

    public function media()
    {
        return $this->hasMany(ChatMedia::class)->where('type', '!=', ChatMedia::TYPE_CHAT_PHOTO);
    }

    public function links()
    {
        return $this->hasMany(ChatMessage::class)->whereNotNull('url');
    }

    public static function byCategory(string $category)
    {
        return static::where('category', 'like', $category.'%');
    }

    public function paymentPlans()
    {
        return $this->hasMany(ChatPaymentPlan::class);
    }

    public static function makeNew(
        array $data,
        Model $reference = null,
        Model $creator = null
    ): Chat {
        if (empty($data['started_at'])) {
            $data['started_at'] = now();
        }

        if (empty($data['permissions']) || !$data['permissions'] instanceof ChatPermissionDTO) {
            $data['permissions'] = (new ChatPermissionDTO())->toArray();
        } elseif ($data['permissions'] instanceof ChatPermissionDTO) {
            $data['permissions'] = $data['permissions']->toArray();
        }

        DB::beginTransaction();

        $chat = new self($data);

        if (!empty($reference)) {
            $chat->reference()->associate($reference);
        }

        if (!empty($creator)) {
            $chat->creator()->associate($creator);
        }

        $chat->save();

        if (!empty($creator)) {
            $chat->addMember($creator, ChatMember::ROLE_OWNER);
        }

        DB::commit();

        return $chat;
    }

    public static function newDirect(
        array $data,
        Model $reference = null,
        Model $creator = null
    ): Chat {
        return static::makeNew(
            array_merge($data, ['type' => static::TYPE_DIRECT]),
            $reference,
            $creator
        );
    }

    public static function newGroup(
        array $data,
        Model $reference = null,
        Model $creator = null
    ): Chat {
        return static::makeNew(
            array_merge($data, ['type' => static::TYPE_GROUP]),
            $reference,
            $creator
        );
    }

    public static function newChannel(
        array $data,
        Model $reference = null,
        Model $creator = null
    ): Chat {
        if (empty($data['permissions']) || !$data['permissions'] instanceof ChatPermissionDTO) {
            $permissions = new ChatPermissionDTO();
            $permissions->sendMessage = false;
            $data['permissions'] = $permissions;
        } elseif ($data['permissions'] instanceof ChatPermissionDTO) {
            $data['permissions']->sendMessage = false;
        }

        return static::makeNew(
            array_merge($data, [
                'type' => static::TYPE_CHANNEL,
            ]),
            $reference,
            $creator
        );
    }

    public function addMemberById(
        $userId,
        string $userMorphClass,
        string $role = ChatMember::ROLE_MEMBER,
        int $invitedById = null,
        int $planId = null
    ): ChatMember {
        if (
            $this->type == static::TYPE_DIRECT &&
            $role === ChatMember::ROLE_MEMBER &&
            $this->members()->count() === 2
        ) {
            throw new MembershipLimitException();
        } elseif (
            !empty($limit = $this->permissions['membership_limit']) &&
            $role === ChatMember::ROLE_MEMBER &&
            $this->members()->count() >= $limit
        ) {
            throw new MembershipLimitException();
        }

        DB::beginTransaction();
        $member = $this->members()
            ->where('user_id', $userId)
            ->where('user_type', $userMorphClass)
            ->first();

        if (empty($member)) {
            $member = new ChatMember(['role' => $role]);
            $member->user_id = $userId;
            $member->user_type = $userMorphClass;
            $member->chat()->associate($this);
        } else {
            $member->role = $role;
        }

        if (!empty($invitedById)) {
            $invitedBy = ChatMember::findOrFail($invitedById);
            if (
                $invitedBy->role === ChatMember::ROLE_MEMBER &&
                $this->permissions['add_member'] === true
            ) {
                $member->invitedBy()->associate($invitedBy);
            } else {
                throw new PermissionException();
            }
        }

        if (!is_null($planId)) {
            $plan = ChatPaymentPlan::findOrFail($planId);
            $now = now();

            if ((int)$plan->chat_id !== (int)$this->id) {
                throw new WrongChatException();
            } elseif ($plan->plan_started_at > $now) {
                throw new NotStartedPlanException();
            } elseif ($plan->plan_expired_at < $now) {
                throw new ExpiredPlanException();
            } elseif (!is_null($plan->quantity)) {
                if ($plan->quantity == 0) {
                    throw new PlanQuantityLimitException();
                } else {
                    $plan->quantity -= 1;
                    $plan->save();
                }
            }

            if (!empty($plan->membership_expired_days_after)) {
                $member->expired_at = now()->addDays($plan->membership_expired_days_after);
            }

            $member->plan()->associate($plan);
        }

        if ($member->isDirty()) {
            $member->save();
        }

        DB::commit();

        return $member;
    }

    public function addMember(
        Model $user,
        string $role = ChatMember::ROLE_MEMBER,
        ChatMember $invitedBy = null,
        ChatPaymentPlan $plan = null
    ): ChatMember {
        if (
            $this->type == static::TYPE_DIRECT &&
            $role === ChatMember::ROLE_MEMBER &&
            $this->members()->count() === 2
        ) {
            throw new MembershipLimitException();
        } elseif (
            !empty($limit = $this->permissions['membership_limit']) &&
            $role === ChatMember::ROLE_MEMBER &&
            $this->members()->count() >= $limit
        ) {
            throw new MembershipLimitException();
        }

        DB::beginTransaction();
        $member = $this->members()
            ->where('user_id', $user->getKey())
            ->where('user_type', $user->getMorphClass())
            ->first();

        if (empty($member)) {
            $member = new ChatMember(['role' => $role]);
            $member->user()->associate($user);
            $member->chat()->associate($this);
        } else {
            $member->role = $role;
        }

        if (!empty($invitedBy)) {
            if (
                $invitedBy->role === ChatMember::ROLE_MEMBER &&
                $this->permissions['add_member'] === true
            ) {
                $member->invitedBy()->associate($invitedBy);
            } else {
                throw new PermissionException();
            }
        }

        if (!empty($plan)) {
            $now = now();

            if ((int)$plan->chat_id !== (int)$this->id) {
                throw new WrongChatException();
            } elseif ($plan->plan_started_at > $now) {
                throw new NotStartedPlanException();
            } elseif ($plan->plan_expired_at < $now) {
                throw new ExpiredPlanException();
            } elseif (!is_null($plan->quantity)) {
                if ($plan->quantity <= 0) {
                    throw new PlanQuantityLimitException();
                } else {
                    $plan->quantity -= 1;
                    $plan->save();
                }
            }

            if (!empty($plan->membership_expired_days_after)) {
                $member->expired_at = now()->addDays($plan->membership_expired_days_after);
            }

            $member->plan()->associate($plan);
        }

        if ($member->isDirty()) {
            $member->save();
        }

        DB::commit();

        return $member;
    }

    public function newChatPhoto(
        string $path,
        string $originalFileName = null,
        string $driver = null,
        bool $deleteOthers = false
    ): ChatMedia {
        DB::beginTransaction();
        if ($deleteOthers) {
            $this->photos()->delete();
        }

        $media = ChatMedia::makeNew(
            $this,
            ChatMedia::TYPE_CHAT_PHOTO,
            $path,
            $originalFileName,
            $driver
        );
        $this->photo()->associate($media);
        $this->save();

        DB::commit();

        return $media;
    }

    public function newUploadChatPhoto(
        string $tempFilePath,
        string $originalFileName = null,
        string $driver = null,
        bool $deleteOthers = false
    ): ChatMedia {
        DB::beginTransaction();
        if ($deleteOthers) {
            $this->photos()->delete();
        }

        $media = ChatMedia::uploadAndMake(
            $this,
            ChatMedia::TYPE_CHAT_PHOTO,
            $tempFilePath,
            $originalFileName,
            $driver
        );
        $this->photo()->associate($media);
        $this->save();

        DB::commit();

        return $media;
    }

    public function newPaymentPlan(array $data, Model $creator = null): ChatPaymentPlan
    {
        $paymentPlan = new ChatPaymentPlan($data);
        $paymentPlan->chat()->associate($this);

        if (!empty($creator)) $paymentPlan->creator()->associate($creator);

        $paymentPlan->save();

        return $paymentPlan;
    }

    public function deleteChat(
        ChatMember $chatMember = null,
        bool $isSystem = false,
        bool $deleteMessages = true
    ): bool {
        if (
            (!empty($chatMember) &&
                $chatMember->role !== ChatMember::ROLE_OWNER) &&
            !$isSystem
        ) {
            throw new PermissionException();
        }

        // check if membership is expired
        if (
            !empty($chatMember) &&
            !empty($chatMember->expired_at) &&
            $chatMember->expired_at < now()
        ) {
            throw new ExpiredMembershipException();
        }

        DB::beginTransaction();

        if ($deleteMessages) {
            $this->messages()->delete();
        }

        $this->delete();

        DB::commit();

        return true;
    }

    public function isMember(Model $user): bool
    {
        return $this->members()
            ->where('user_id', $user->getKey())
            ->where('user_type', $user->getMorphClass())
            ->count() ? true : false;
    }

    public function canView(Model $user = null): bool
    {
        $canView = $this->permissions['allow_spectators'];

        if ($user) {
            $canView = $this->members()
                ->where('user_id', $user->getKey())
                ->where('user_type', $user->getMorphClass())
                ->count() ? true : false;
        }

        return $canView;
    }
}
